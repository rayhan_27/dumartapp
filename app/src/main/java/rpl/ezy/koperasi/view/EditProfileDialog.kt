package rpl.ezy.koperasi.view

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.dialog_edit_profile.*
import rpl.ezy.koperasi.R

class EditProfileDialog(context: Context) : Dialog(context){

    init {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_edit_profile)

        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp  = WindowManager.LayoutParams()
        val window = window
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = lp

        btn_close.setOnClickListener {
            dismiss()
        }

        outside.setOnClickListener {
            dismiss()
        }
    }
}