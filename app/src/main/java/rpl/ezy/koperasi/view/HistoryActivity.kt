package rpl.ezy.koperasi.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_history.*
import rpl.ezy.koperasi.R
import rpl.ezy.koperasi.adapter.*

class HistoryActivity : AppCompatActivity() {

    private val mDataShow = listOf(
        DataHistory("Dasi", 23, "08 - 07 - 2019",1),
        DataHistory("Buku", 42, "12 - 06 - 2019",1),
        DataHistory("Pensil", 52, "23 - 07 - 2019",1),
        DataHistory("Penghapus", 123, "21 - 08 - 2019", 2),
        DataHistory("Bulpen", 53, "30 - 07 - 2019", 2),
        DataHistory("Penggaris", 20, "11 - 06 - 2019", 2)
    )

    private val mBulan = listOf(
        DataMonth("Januari"),
        DataMonth("Februari"),
        DataMonth("Maret"),
        DataMonth("April"),
        DataMonth("Mei"),
        DataMonth("Juni"),
        DataMonth("Juli"),
        DataMonth("Agustus"),
        DataMonth("September"),
        DataMonth("Oktober"),
        DataMonth("November"),
        DataMonth("Desember")

    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        setSupportActionBar(toolbar)
        toolbar.navigationIcon = resources.getDrawable(R.drawable.back_white)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white_1))
        toolbar.setNavigationOnClickListener {
            finish()
        }

        val window = this.window
        window.statusBarColor = ContextCompat.getColor(this, R.color.green_1)

        rv_month.apply {
            layoutManager = LinearLayoutManager(this@HistoryActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = MonthAdapter(mBulan, this@HistoryActivity)
        }

        rv_show.apply {
            layoutManager = LinearLayoutManager(this@HistoryActivity)
            adapter = HistoryAdapter(mDataShow, this@HistoryActivity)
        }
    }
}
