package rpl.ezy.koperasi.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import kotlinx.android.synthetic.main.activity_stats.*
import rpl.ezy.koperasi.R

@Suppress("DEPRECATION")
class StatsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)
        setSupportActionBar(toolbar)
        toolbar.navigationIcon = resources.getDrawable(R.drawable.back_black)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black_1))
        toolbar.setNavigationOnClickListener {
            finish()
        }

        val window = this.window
        window.statusBarColor = ContextCompat.getColor(this, R.color.green_1)

        mostBarChart()
        topBarChart()
    }

    private fun mostBarChart() {
        val entries = ArrayList<BarEntry>()
        entries.add(BarEntry(0f, 6f))
        entries.add(BarEntry(1f, 5f))
        entries.add(BarEntry(2f, 4f))
        entries.add(BarEntry(3f, 3f))
        entries.add(BarEntry(4f, 2f))
        entries.add(BarEntry(5f, 1f))

        val dataSet = BarDataSet(entries, "Customized values")
        dataSet.color = ContextCompat.getColor(this, R.color.colorPrimary)
        dataSet.valueTextColor = ContextCompat.getColor(this,
            R.color.colorPrimaryDark
        )

        //****
        // Controlling X axis
        val xAxis = bar_most.xAxis
        // Set the xAxis position to bottom. Default is top
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        //Customizing x axis value
        val months = arrayOf("Buku", "Bulpen", "Pensil", "Penghapus", "Topi", "Penggaris", "dll.")
        xAxis.valueFormatter = IndexAxisValueFormatter(months)
        bar_most.setVisibleXRangeMaximum(7F)
        bar_most.setScaleEnabled(false)

        //***
        // Controlling right side of y axis
        val yAxisRight = bar_most.axisRight
        yAxisRight.isEnabled = false

        //***
        // Controlling left side of y axis
        val yAxisLeft = bar_most.axisLeft
        yAxisLeft.granularity = 1f

        // Setting Data
        val data = BarData(dataSet)
        bar_most.data = data
        bar_most.animateX(2500)
        //refresh
        bar_most.invalidate()
    }

    private fun topBarChart() {
        val entries = ArrayList<BarEntry>()
        entries.add(BarEntry(0f, 6f))
        entries.add(BarEntry(1f, 5f))
        entries.add(BarEntry(2f, 4f))
        entries.add(BarEntry(3f, 3f))
        entries.add(BarEntry(4f, 2f))
        entries.add(BarEntry(5f, 1f))

        val dataSet = BarDataSet(entries, "Customized values")
        dataSet.color = ContextCompat.getColor(this, R.color.colorPrimary)
        dataSet.valueTextColor = ContextCompat.getColor(this,
            R.color.colorPrimaryDark
        )

        //****
        // Controlling X axis
        val xAxis = bar_top.xAxis
        // Set the xAxis position to bottom. Default is top
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        //Customizing x axis value
        val months = arrayOf("Buku", "Bulpen", "Pensil", "Penghapus", "Topi", "Penggaris", "dll.")
        xAxis.valueFormatter = IndexAxisValueFormatter(months)
        bar_top.setVisibleXRangeMaximum(7F)
        bar_top.setScaleEnabled(false)

        //***
        // Controlling right side of y axis
        val yAxisRight = bar_top.axisRight
        yAxisRight.isEnabled = false

        //***
        // Controlling left side of y axis
        val yAxisLeft = bar_top.axisLeft
        yAxisLeft.granularity = 1f

        // Setting Data
        val data = BarData(dataSet)
        bar_top.data = data
        bar_top.animateX(2500)
        //refresh
        bar_most.invalidate()
    }
}
