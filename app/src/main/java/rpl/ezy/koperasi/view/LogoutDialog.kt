package rpl.ezy.koperasi.view

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.dialog_logout.*
import rpl.ezy.koperasi.R

class LogoutDialog(context: Context) : Dialog(context) {

    init {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_logout)

        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        val window = window
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = lp

        bt_no.setOnClickListener {
            dismiss()
        }
        bt_yes.setOnClickListener {
            (context as Activity).startActivity(Intent(context, LoginActivity::class.java))
            (context as Activity).finish()
        }
    }
}
