package rpl.ezy.koperasi.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_forgot_pass.*
import rpl.ezy.koperasi.R

class ForgotPassActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)
        onClick()
    }

    fun onClick(){
        send_email.setOnClickListener {
            card_email.visibility = View.GONE
            card_kode.visibility = View.VISIBLE
        }

        send_kode.setOnClickListener {
            card_kode.visibility = View.GONE
            card_password.visibility = View.VISIBLE
        }

        send_password.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}
