package rpl.ezy.koperasi.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import rpl.ezy.koperasi.R


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        show.setOnClickListener {
            startActivity(Intent(this, ShowActivity::class.java))
        }
        pendapatan.setOnClickListener {
            CountingDialog(this).show()
        }
        history.setOnClickListener {
            startActivity(Intent(this, HistoryActivity::class.java))
        }
        add_item.setOnClickListener {
            AdditemDialog(this).show()
        }
        statistik.setOnClickListener {
            startActivity(Intent(this, StatsActivity::class.java))
        }
        barang_keluar.setOnClickListener {
            ItemOutDialog(this).show()
        }
        logout.setOnClickListener {
            LogoutDialog(this).show()
        }

        username.setOnClickListener {
            EditProfileDialog(this).show()
        }

        val window = this.window
        window.statusBarColor = ContextCompat.getColor(this, R.color.green_1)
    }
}
