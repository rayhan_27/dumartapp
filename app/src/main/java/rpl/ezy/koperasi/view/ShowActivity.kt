package rpl.ezy.koperasi.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_show.*
import androidx.recyclerview.widget.LinearLayoutManager
import rpl.ezy.koperasi.R
import rpl.ezy.koperasi.adapter.DataShow
import rpl.ezy.koperasi.adapter.ShowAdapter


class ShowActivity : AppCompatActivity() {

    private val mDataShow = listOf(
        DataShow("Dasi", 20, 35000),
        DataShow("Sabuk", 20, 40000),
        DataShow("Buku", 200, 2500),
        DataShow("Pensil", 50, 3500),
        DataShow("Penghapus", 55, 2000)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show)
        setSupportActionBar(toolbar)
        toolbar.navigationIcon = resources.getDrawable(R.drawable.back_black)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        val window = this.window
        window.statusBarColor = ContextCompat.getColor(this, R.color.green_1)

        initView()
    }

    private fun initView() {
        rv_show.apply {
            layoutManager = LinearLayoutManager(this@ShowActivity)
            adapter = ShowAdapter(mDataShow, this@ShowActivity)
        }
        back.setOnClickListener {
            finish()
        }
    }
}
