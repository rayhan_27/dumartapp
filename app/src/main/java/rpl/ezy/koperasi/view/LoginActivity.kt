package rpl.ezy.koperasi.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rpl.ezy.koperasi.R
import rpl.ezy.koperasi.api.GetDataService
import rpl.ezy.koperasi.api.RetrofitClient
import rpl.ezy.koperasi.response.Constants.ADMIN
import rpl.ezy.koperasi.response.Constants.EMAIL
import rpl.ezy.koperasi.response.Constants.ID_JABATAN
import rpl.ezy.koperasi.response.Constants.LEVEL
import rpl.ezy.koperasi.response.Constants.SUPER_ADMIN
import rpl.ezy.koperasi.response.Constants.USERNAME
import rpl.ezy.koperasi.response.Constants.USER_ID
import rpl.ezy.koperasi.response.ResponseLogin
import rpl.ezy.koperasi.utils.SharedPreference

class LoginActivity : AppCompatActivity() {

    private var sharedPreferences: SharedPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        sharedPreferences = SharedPreference(this@LoginActivity)

        if (sharedPreferences!!.getIntSharedPreferences(ID_JABATAN) != -1) {
            if (sharedPreferences!!.getIntSharedPreferences(ID_JABATAN) == ADMIN) {
//                startActivity(Intent(this@LoginActivity, AdminActivity::class.java))
//                finish()
                return
            }
            if (sharedPreferences!!.getIntSharedPreferences(ID_JABATAN) == SUPER_ADMIN) {
//                startActivity(Intent(this@Authentification, UserActivity::class.java))
//                finish()
                return
            }
        }

        onClick()
    }

    fun onClick() {

        sharedPreferences = SharedPreference(this)

        login.setOnClickListener {
//            if(ValidationLogin()) {
                ResponseLogin(et_email.text.toString().trim(), et_password.text.toString().trim())
//            }
        }

        forgot_pass.setOnClickListener {
            startActivity(Intent(this, ForgotPassActivity::class.java))
        }
    }

    fun ResponseLogin(username: String, password: String) {
        val service =
            RetrofitClient().getRetrofitInstance().create(GetDataService::class.java)
        val call = service.userLogin(username, password)
        call.enqueue(object : Callback<ResponseLogin> {
            override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                Toast.makeText(
                    this@LoginActivity,
                    "Something went wrong...Please try later!",
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("LOGLOGAN", "${t.message}")
            }

            override fun onResponse(call: Call<ResponseLogin>, response: Response<ResponseLogin>) {
//                if (response.isSuccessful) {

                    val res = response.body()

                    if (res!!.message == "Login success") {
                        Toast.makeText(
                            this@LoginActivity,
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()

                        val data = response.body()!!.user

                        SetDataUser(USER_ID, data.id_user, "")
                        SetDataUser(USERNAME, 0, data.username)
                        SetDataUser(EMAIL, 0, data.email)
                        SetDataUser(ID_JABATAN, data.id_jabatan, "")
                        SetDataUser(LEVEL, data.level, "")

                        if (data.level == ADMIN) {
                            SetDataUser(LEVEL, ADMIN, "")
                            Toast.makeText(this@LoginActivity, "ADMIN", Toast.LENGTH_SHORT).show()
//                        startActivity(Intent(this@LoginActivity, AdminActivity::class.java))
//                        (context as Activity).finish()
                            return
                        }

                        SetDataUser(LEVEL, SUPER_ADMIN, "")
                        Toast.makeText(this@LoginActivity, "ADMIN", Toast.LENGTH_SHORT).show()
                    }
//                } else {
//                    Toast.makeText(
//                        this@LoginActivity,
//                        response.body()!!.message,
//                        Toast.LENGTH_SHORT
//                    ).show()
//                }
            }

        })
    }

    fun SetDataUser(key: String, int: Int, string: String) {
        if (string != "") {
            sharedPreferences!!.setSharedPreferences(key, string)
        } else {
            sharedPreferences!!.setSharedPreferences(key, int)
        }
    }

    fun ValidationLogin(): Boolean {
        if (et_email?.text?.isEmpty()!!) {
            et_email?.error = "Enter Email"
            et_email?.requestFocus()
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(et_email?.text).matches()) {
            et_email?.error = "Enter correct Email!"
            et_email?.requestFocus()
            return false
        }
        if (et_password?.text?.isEmpty()!!) {
            et_password?.error = "Enter Password"
            et_password?.requestFocus()
            return false
        }
        if (et_password?.length()!! < 8) {
            et_password?.error = "At least 8 characters!"
            et_password?.requestFocus()
            return false
        }

        return true
    }
}
