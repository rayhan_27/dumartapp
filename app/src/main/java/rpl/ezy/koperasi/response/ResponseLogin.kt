package rpl.ezy.koperasi.response

import rpl.ezy.koperasi.model.MUser
import java.io.Serializable
import com.google.gson.annotations.SerializedName


class ResponseLogin(
    var status : Int,
    var error : Any,
    var message : String,
    var token : String,
    var user : MUser
): Serializable