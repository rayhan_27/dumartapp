package rpl.ezy.koperasi.response

object Constants {
    const val ADMIN = 1
    const val SUPER_ADMIN = 2

    const val TOKEN = "token"
    const val USER_ID = "user_id"
    const val USERNAME = "username"
    const val EMAIL = "email"
    const val ID_JABATAN = "id_jabatan"
    const val LEVEL = "level"
}