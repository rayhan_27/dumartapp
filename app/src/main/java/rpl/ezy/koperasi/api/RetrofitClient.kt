package rpl.ezy.koperasi.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    fun getRetrofitInstance(): Retrofit {

        val retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.1.6/dumart/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
//        }
        return retrofit
    }
}