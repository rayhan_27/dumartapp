package rpl.ezy.koperasi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_show_data.view.*
import rpl.ezy.koperasi.R
import java.text.NumberFormat
import java.util.*


class ShowAdapter(val data : List<DataShow>, context: Context):
    RecyclerView.Adapter<ShowAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_show_data, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val localId = Locale("in", "ID")
        val format = NumberFormat.getCurrencyInstance(localId)

        holder.mNama.text = data[position].nama
        holder.mStok.text = "Stok : " + data[position].stok.toString()
//        holder.mHarga.text = data[position].harga.toString()
        holder.mHarga.text = format.format(data[position].harga)

        holder.mMore.setOnClickListener {
            holder.cardHide.visibility = View.VISIBLE
            holder.mMore.apply {
                !isClickable
                !isFocusable
            }
            if(holder.cardHide.isVisible) {
                holder.mMoreHide.setOnClickListener {
                    holder.cardHide.visibility = View.GONE
                }
            }

        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var mNama   = view.nama
        var mStok   = view.stok
        var mHarga  = view.harga
        var mMore  = view.more
        var mMoreHide  = view.more_hide
        var cardHide = view.card_select
    }
}