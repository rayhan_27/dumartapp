package rpl.ezy.koperasi.adapter

data class DataHistory(val nama: String, val stok: Int, val tanggal: String, val kode: Int)