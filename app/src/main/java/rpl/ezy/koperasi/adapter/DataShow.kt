package rpl.ezy.koperasi.adapter

data class DataShow(val nama: String, val stok: Int, val harga: Int)