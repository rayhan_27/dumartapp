package rpl.ezy.koperasi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_history_data.view.*
import rpl.ezy.koperasi.R

class HistoryAdapter(val data : List<DataHistory>, context: Context):
    RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_history_data, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.mNama.text = data[position].nama
        holder.mStok.text = "Stok : " + data[position].stok.toString()
        holder.mTanggal.text = data[position].tanggal


        if(data[position].kode == 1) {
            holder.iteminout.setBackgroundResource(R.drawable.bg_item_code_in)
        }
        if(data[position].kode == 2) {
            holder.iteminout.setBackgroundResource(R.drawable.bg_item_code_out)
        }
        holder.mMore.setOnClickListener {
            holder.cardHide.visibility = View.VISIBLE
            holder.mMore.apply {
                !isClickable
                !isFocusable
            }
            if(holder.cardHide.isVisible) {
                holder.mMoreHide.setOnClickListener {
                    holder.cardHide.visibility = View.GONE
                }
            }

        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var mNama   = view.nama
        var mStok   = view.stok
        var mTanggal = view.tanggal
        var mMore  = view.more
        var mMoreHide  = view.more_hide
        var cardHide = view.card_select
        val iteminout = view.item_inout
    }
}