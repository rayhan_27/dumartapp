package rpl.ezy.koperasi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_history_bulan.view.*
import rpl.ezy.koperasi.R

class MonthAdapter(val data: List<DataMonth>, val context: Context): RecyclerView.Adapter<MonthAdapter.ViewHolder>() {

    val selector = -1
    var selected = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_history_bulan, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bulan.text = data[position].bulan
        holder.itemView.isSelected = selector == position
        if (position == selected){
            holder.master.setBackgroundResource(R.drawable.bg_show_data)
            holder.bulan.setTextColor(ContextCompat.getColor(context, R.color.black_1))
        } else {
            holder.master.setBackgroundResource(R.drawable.bg_item_rv)
            holder.bulan.setTextColor(ContextCompat.getColor(context, R.color.white_1))
        }
        holder.itemView.setOnClickListener {
            Toast.makeText(context, data[position].bulan, Toast.LENGTH_SHORT).show()
            selected = position
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var bulan = view.bulan
        var master = view.master
    }
}