package rpl.ezy.koperasi.model

class MUser(
    var id_user: Int,
    var username: String,
    var email: String,
    var id_jabatan: Int,
    var level: Int
)